CC = gcc
CXX = g++

#Compiler Flags
CFLAGS = -g -Wall -Wextra -Wpedantic -std=c2x
CXXFLAGS = -g -Wall -Wextra -Wpedantic -std=c++20

#Linker Flags
STD_FLAGS = m
SDL_FLAGS = SDL2
GL_FLAGS = GLEW GL GLU

#Liker Substitution
GL_LIBS = $(patsubst %,-l%,$(GL_FLAGS))
SDL_LIBS = $(patsubst %,-l%,$(SDL_FLAGS))
STD_LIBS = $(patsubst %,-l%,$(STD_FLAGS))

LDFLAGS = $(SDL_LIBS) $(GL_LIBS) $(STD_LIBS)

#Output Directories
OBJ_DIR = ./obj
BIN_DIR = ./bin

#Source Directories
C_SRCS := $(wildcard **/*.c)
CXX_SRCS := $(wildcard **/*.cpp)

#Obj Directories
C_OBJS = $(patsubst %.c,$(OBJ_DIR)/%.o,$(C_SRCS))
CXX_OBJS = $(patsubst %.cpp,$(OBJ_DIR)/%.o,$(CXX_SRCS))

# Executable name
OBJ_NAME = out

$(OBJ_DIR)/%.o: %.c
	@mkdir -p $(OBJ_DIR)/$(dir $<)
	$(CC) $(CFLAGS) -c $< -o $@

$(OBJ_DIR)/%.o: %.cpp
	@mkdir -p $(OBJ_DIR)/$(dir $<)
	$(CXX) $(CXXFLAGS) -c $< -o $@

# Build rule for the final executable
all: $(C_OBJS) $(CXX_OBJS)
	mkdir -p $(OBJ_DIR)
	$(CXX) $(C_OBJS) $(CXX_OBJS) $(CFLAGS) $(CXXFLAGS) $(LDFLAGS) -o $(BIN_DIR)/$(OBJ_NAME)

# Run the executable
run: all
	./bin/$(OBJ_NAME)

# Clean up object files and the executable
clean:
	rm -rf $(OBJ_DIR)/* $(BIN_DIR)/$(OBJ_NAME)

# Run tests
test:
	# Add your test command here

# Mark phony targets
.PHONY: all run clean test
