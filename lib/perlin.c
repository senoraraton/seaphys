#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "./perlin.h"
#include "./murmur.h"

float fade(float t) {
     return t * t * t * (t * (t * 6 - 15) + 10);
}

float lerp(float t, float a, float b) {
    return a + t * (b - a);
}

float gradientDirection3D(int hash, float x, float y, float z) {
    int h = hash & 15;
    float u,v;

    if (h < 8) {
        u = x;
    } else {
        u = y;
    }

    if (h < 4) {
        v = y;
    } else if (h == 12 || h ==14) {
        v = x;
    } else {
        v = z;
    }

    return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
}

float gradient2D(uint32_t hash, float x, float y) {
    switch (hash & 3) {
        case 0:
            return x;
        case 1:
            return y;        
        case 2:
            return -x;
        case 3:
            return -y;
        default:
            return 0.0; //This is superfulous
    }
}

/*float perlin2DPoint(float x, float y, uint32_t seed) {*/
    /*//Determine cords*/
    /*int xi = (int)x;*/
    /*int yi = (int)y;*/
    /*//Determine the offsets*/
    /*float xf = x - xi;*/
    /*float yf = y - yi;*/
    /*//Smooth output*/
    /*float u = fade(xi);*/
    /*float v = fade(yi);*/
    /*//Hash corners*/
    /*uint32_t AB = murmurHash2(xi, yi, seed);*/
    /*uint32_t BC = murmurHash2(xi + 1, yi, seed); */
    /*uint32_t CD = murmurHash2(xi +1, yi +1, seed);*/
    /*uint32_t AD = murmurHash2(xi, yi +1, seed);*/
    /*//Calulate Gradients*/
    /*float gradAB = gradient2D(AB, xf, yf);*/
    /*float gradBC = gradient2D(BC, xf - 1, yf);*/
    /*float gradCD = gradient2D(CD, xf - 1, yf - 1);*/
    /*float gradAD = gradient2D(AD, xf, yf - 1);*/
    /*//Dot products*/
    /*[>float dotAB = dotProduct2D(AB, xi, yi);<]*/
    /*[>float dotBC = dotProduct2D(BC, xi + 1, yi);<]*/
    /*[>float dotCD = dotProduct2D(CD, xi + 1, yi + 1);<]*/
    /*[>float dotAD = dotProduct2D(AD, xi, yi + 1);<]*/
    /*[>//Lerp<]*/
    /*[>float interpXTop = lerp(u, dotAB, dotBC);<]*/
    /*[>float interpXBot = lerp(u, dotAD, dotCD);<]*/
    /*[>float finalValue = lerp(v, interpXBot, interpXTop);<]*/
    /*[>return finalValue;<]*/
    /*return lerp(v, lerp(u, gradAB, gradBC),*/
                 /*lerp(u, gradCD, gradAD));*/
/*}*/

float perlin2DPoint(float x, float y, uint32_t seed) {
    // Determine coords
    int xi = (int)x;
    int yi = (int)y;
    // Determine the offsets
    float xf = x - xi;
    float yf = y - yi;
    // Smooth output
    float u = fade(xi);
    float v = fade(yi);
    // Hash corners
    uint32_t AB = murmurHash2(xi, yi, seed);
    uint32_t BC = murmurHash2(xi + 1, yi, seed);
    uint32_t CD = murmurHash2(xi + 1, yi + 1, seed);
    uint32_t AD = murmurHash2(xi, yi + 1, seed);
    // Calculate Dot Products with Gradients
    float dotAB = gradient2D(AB, xf, yf) * xf + gradient2D(AB, xf, yf) * yf;
    float dotBC = gradient2D(BC, xf - 1, yf) * (xf - 1) + gradient2D(BC, xf, yf) * yf;
    float dotCD = gradient2D(CD, xf - 1, yf - 1) * (xf - 1) + gradient2D(CD, xf - 1, yf - 1) * (yf - 1);
    float dotAD = gradient2D(AD, xf, yf - 1) * xf + gradient2D(AD, xf, yf - 1) * (yf - 1);
    // Lerp
    float interpXTop = lerp(u, dotAB, dotBC);
    float interpXBot = lerp(u, dotAD, dotCD);
    float finalValue = lerp(v, interpXBot, interpXTop);
    return finalValue;
}

PerlinNoise2D* create2DArray(PerlinNoise2D* perlinMatrix) {
    float** array = (float**)malloc(perlinMatrix->height * sizeof(float*));
    for (int i = 0; i < perlinMatrix->height; ++i) {
        array[i] = (float*)malloc(perlinMatrix->width * sizeof(float));
        for (int j = 0; j < perlinMatrix->width; ++j) {
            perlinMatrix->perlinMatrix[i][j] = perlin2DPoint(i, j, perlinMatrix->seed);  
        }
    }
    return perlinMatrix;
}

float perlin3DPoint(float x, float y, float z) {
}

PerlinNoise3D* create3DArray(PerlinNoise3D* perlinMatrix) {
    float*** array = (float***)malloc(perlinMatrix->depth * sizeof(float**));
    for (int i = 0; i < perlinMatrix->depth; ++i) {
        array[i] = (float**)malloc(perlinMatrix->height * sizeof(float*));
        for (int j = 0; j < perlinMatrix->height; ++j) {
            array[i][j] = (float*)malloc(perlinMatrix->width * sizeof(float));
            for (int k = 0; k < perlinMatrix->width; ++k) {
                perlinMatrix->perlinCube[i][j][k] = perlin3DPoint(i, j, k);  
            }
        }
    }
    return perlinMatrix;
}

void deallocate3d(PerlinNoise3D* perlinCube) {
        if (perlinCube->perlinCube) {
            for (int i = 0; i < perlinCube->width; i++) {
                for (int j = 0; j < perlinCube->height; j++) {
                    free(perlinCube->perlinCube[i][j]);
                }
                free(perlinCube->perlinCube[i]);
            }
            free(perlinCube->perlinCube);
        }
        free(perlinCube);
}

void deallocate2d(PerlinNoise2D* perlinMatrix) {
    if (perlinMatrix->perlinMatrix) {
            for (int i = 0; i < perlinMatrix->width; i++) {
                free(perlinMatrix->perlinMatrix[i]);
            }
            free(perlinMatrix->perlinMatrix);
        }
        free(perlinMatrix);
}
