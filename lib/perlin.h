#ifndef PERLIN_H
#define PERLIN_H

#include <stdint.h>

//Initalize Permutation Array
void initPerlin();

typedef struct {
    int width;
    int height;
    float** perlinMatrix;
    uint32_t seed;
} PerlinNoise2D;

typedef struct {
    int width;
    int height;
    int depth;
    float*** perlinCube;
    uint32_t seed;
} PerlinNoise3D;

//Generate 2D perlin point
float perlin2DPoint(float x, float y, uint32_t seed);

//Generate 2d Perlin matrix
PerlinNoise2D* perlin2D(PerlinNoise2D* perlinMatrix);

//Free 2d Perlin matrix
void deallocate2D(PerlinNoise2D* perlinMatrix);

//Generate 3D perlin point
float perlin3DPoint(float x, float y, float z);

//Generate 3d Perlin cube
PerlinNoise3D* perlin3D(PerlinNoise3D* perlinCube);

//Free 3d Perlin cube
void deallocate3D(PerlinNoise3D* perlinCube);

#endif /* PERLIN_H */
