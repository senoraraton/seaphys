#include "./murmur.h"

static const uint32_t MURMUR_C1 = 0xcc9e2d51;
static const uint32_t MURMUR_C2 = 0x1b873593;
static const uint32_t MURMUR_R1 = 15;
static const uint32_t MURMUR_R2 = 13;
static const uint32_t MURMUR_M = 5;
static const uint32_t MURMUR_N = 0xe6546b64;

uint32_t murmurMix(uint32_t value) {
    value ^= value >> MURMUR_R1;
    value *= MURMUR_C1;
    value ^= value >> MURMUR_R2;
    value *= MURMUR_C2;

    return value;
}

uint32_t murmurHash2(uint32_t x, uint32_t y, uint32_t seed) {
    uint32_t hash = seed;
    
    hash ^= murmurMix(x);
    hash = (hash << MURMUR_R1) | (hash >> (32 - MURMUR_R1));
    hash = hash * MURMUR_M + MURMUR_N;

    hash ^= murmurMix(y);
    hash = (hash << MURMUR_R1) | (hash >> (32 - MURMUR_R1));
    hash = hash * MURMUR_M + MURMUR_N;

    return hash;
}

uint32_t murmurHash3(uint32_t x, uint32_t y, uint32_t z, uint32_t seed) {
    uint32_t hash = seed;

    hash ^= murmurMix(x);
    hash = (hash << MURMUR_R1) | (hash >> (32 - MURMUR_R1));
    hash = hash * MURMUR_M + MURMUR_N;

    hash ^= murmurMix(y);
    hash = (hash << MURMUR_R1) | (hash >> (32 - MURMUR_R1));
    hash = hash * MURMUR_M + MURMUR_N;

    hash ^= murmurMix(z);
    hash = (hash << MURMUR_R1) | (hash >> (32 - MURMUR_R1));
    hash = hash * MURMUR_M + MURMUR_N;

    return hash;
}
