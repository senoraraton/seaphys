#ifndef MURMUR_H
#define MURMUR_H

#include <stdint.h>

uint32_t murmurHash2(uint32_t x, uint32_t y, uint32_t seed);
uint32_t murmurHash3(uint32_t x, uint32_t y, uint32_t z, uint32_t seed);

#endif /* MURMUR_H*/
