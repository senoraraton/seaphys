#ifndef SPEHRICALREGION_H
#define SPEHRICALREGION_H
#include <array>
#include <memory>
#include <cstring>
#include "SphericalCoord.h"

//The coordinate array is sorted in a counter-clockwise orientation
// (x,y),(x+1, y),(x+1, y+1),(x, y+1)

class SphericalRegion {
    private:
        void calculateArcLengths() {
            double delta_theta = coordinates[1]->theta - coordinates[0]->theta;
            double delta_phi = coordinates[2]->phi - coordinates[1]->phi;

            arc_length_x = coordinates[0]->radius * delta_theta; 
            arc_length_y = coordinates[0]->radius * delta_phi; 
        }

    public:
        std::array<std::shared_ptr<SphericalCoord>, 4> coordinates;
        double arc_length_x;
        double arc_length_y;

        SphericalRegion(std::array<std::shared_ptr<SphericalCoord>, 4> c) 
            : coordinates(c) {
            calculateArcLengths();
        }

        std::array<std::array<double,3>, 4> getCartesianCords() const {
            std::array<std::array<double, 3>, 4> c;
            for (size_t index = 0; index < coordinates.size(); ++index) {
                c[index] = coordinates[index]->getCartesianCoords();
            }
            return c;
        }
        std::array<std::array<double,3>, 4> getSphericalCords() const {
            std::array<std::array<double,3>, 4> c;
            for (size_t index = 0; index < coordinates.size(); ++index) {
                c[index][0] = coordinates[index]->radius;
                c[index][1] = coordinates[index]->phi;
                c[index][2] = coordinates[index]->theta;   
            }
            return c;
        }
};

#endif
