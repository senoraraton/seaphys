#include <cmath>
#include <vector>
#include "Sphere.h"
#include "SphericalCoord.h"

Sphere::Sphere(double r, int sectors, int stacks, int depths)
    : radius(r), numSectors(sectors), numStacks(stacks), numDepths(depths) {
        sphereCords = generateCoordinates();
    }

std::vector<std::vector<SphericalSection>> Sphere::generateCoordinates(double terminationPoint) {
    std::vector<std::vector<SphericalSection>> sphere;
    double currentRadius = radius;
    for (int depth = 0; depth < numDepths; ++depth) {

        //Ideally here we should probably grab the last chunk, this just leaves a void
        if (currentRadius <= terminationPoint) {
            break;
        }

        for (int x = 0; x < numSectors; x++) {
            std::vector<SphericalSection> row;
            double phi = (2.0 * M_PI / numSectors) * x;
            for (int y = 0; y < numStacks; y++) {

                double theta = (2.0 * M_PI / numStacks) * y; 

                SphericalCoord point(currentRadius, phi, theta);
            }
            sections.push_back(row);
        }

        double currentRadius = currentRadius - (radius/numDepths);
    }

    return sphere;
}
