#ifndef SPHERESECTION_H
#define SPHERESECTION_H

#include <cmath>
#include <cstring>
#include <array>
#include "SphericalRegion.h"



class SphericalSection {
public:
    const SphericalRegion& top;
    const SphericalRegion& bot;

    SphericalSection(SphericalRegion& t, SphericalRegion& b) : top(t), bot(b)  {}
};

#endif
