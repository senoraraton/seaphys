#ifndef SPEHRICALCORD_H
#define SPEHRICALCORD_H
#include <array>
#include <cmath>

class SphericalCoord {
    public:
        double radius;
        double phi;
        double theta;
        std::array<double, 3> getCartesianCoords() {
            std::array<double, 3> coordinates;
            coordinates[0] = radius * sin(phi) * cos(theta);  // x
            coordinates[1] = radius * sin(phi) * sin(theta);  // y
            coordinates[2] = radius * cos(phi);               // z
        return coordinates;
        }
        SphericalCoord(double r, double p, double t) :radius(r), phi(p), theta(t) {}
};

#endif
