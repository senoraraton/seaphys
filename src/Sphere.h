#ifndef SPHERE_H
#define SPHERE_H

#include <vector>
#include "SphericalSection.h"

class Sphere {
    private:
        std::vector<std::vector<SphericalSection>> generateCoordinates(double terminationRadius = 0.0);

    public:
        double radius;
        int numSectors;
        int numStacks;
        int numDepths;
        std::vector<std::vector<SphericalSection>> sphereSections;
        Sphere(double r, int sectors, int stacks, int depths);

};

#endif
